using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mouvement : MonoBehaviour
{
    public float vitesse = 0.01f;
    public float vitesseSprint = 0.02f;
    public float forceSaut = 5f;
    private bool estAuSol = true;

    void Update()
    {
        float vitesseActuelle = (Input.GetKey(KeyCode.LeftShift)) ? vitesseSprint : vitesse;

        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Translate(Vector3.forward * vitesseActuelle);
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Translate(Vector3.back * vitesseActuelle);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(Vector3.up, -2);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(Vector3.up, 2);
        }
        if (Input.GetKeyDown(KeyCode.Space) && estAuSol)
        {
            GetComponent<Rigidbody>().AddForce(Vector3.up * forceSaut, ForceMode.Impulse);
            estAuSol = false;
        }
    }
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            estAuSol = true;
        }
    }
}