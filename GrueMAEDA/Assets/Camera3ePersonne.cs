using UnityEngine;

public class Camera3ePersonne : MonoBehaviour
{
    public Transform cible;       // La grue que la caméra va suivre
    public Vector3 offset = new Vector3(0, 5, -10); // Décalage pour placer la caméra derrière et au-dessus
    public float vitesseSuivi = 5f; // Vitesse de suivi

    void LateUpdate()
    {
        // Position souhaitée de la caméra avec le décalage
        Vector3 positionCible = cible.position + offset;

        // Déplacer la caméra progressivement vers la position souhaitée
        transform.position = Vector3.Lerp(transform.position, positionCible, vitesseSuivi * Time.deltaTime);

        // Toujours orienter la caméra vers la grue
        transform.LookAt(cible);
    }
}
