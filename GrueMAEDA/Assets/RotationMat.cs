using UnityEngine;

public class RotationMat : MonoBehaviour
{
    public float vitesseRotation = 100f; // Vitesse de rotation du mât

    void Update()
    {
        // Rotation du mât vers la gauche avec la touche flèche gauche
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(Vector3.up, -vitesseRotation * Time.deltaTime);
        }
        // Rotation du mât vers la droite avec la touche flèche droite
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(Vector3.up, vitesseRotation * Time.deltaTime);
        }
    }
}
