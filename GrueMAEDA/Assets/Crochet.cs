using UnityEngine;

public class Crochet : MonoBehaviour
{
    private FixedJoint joint; // Référence au joint du crochet
    private ArticulationBody targetBody; // Référence à l'ArticulationBody de l'objet à accrocher

    void OnCollisionEnter(Collision collision)
    {
        // Vérifie que le crochet ne tient pas déjà un objet et que l'objet en collision a un ArticulationBody
        if (joint == null && collision.gameObject.GetComponent<ArticulationBody>() != null)
        {
            targetBody = collision.gameObject.GetComponent<ArticulationBody>();

            // Ajoute un FixedJoint pour accrocher l'objet
            joint = gameObject.AddComponent<FixedJoint>();
            joint.connectedArticulationBody = targetBody;
            joint.breakForce = Mathf.Infinity; // Rend le joint très résistant aux forces
            Debug.Log("Objet attaché");
        }
    }

    void Update()
    {
    
    }

    void LacherObjet() // appuyez sur espace pour lacher l'objet
    {
        if (joint != null)
        {
            Destroy(joint); // Détruit le FixedJoint pour relâcher l'objet
            joint = null;
            targetBody = null;
            Debug.Log("Objet relâché");
        }
    }
}
