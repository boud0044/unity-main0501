using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationCommande : MonoBehaviour
{
    public GameObject Rotation; // Objet auquel appliquer la rotation
    public string axe; // Axe de rotation défini
    public int Sens = 1; // Sens de rotation (1 pour normal, -1 pour inversé)

    void Update()
    {
        // Récupère l'entrée utilisateur pour l'axe de rotation
        float input = Input.GetAxis(axe);
        EtatRotation rotationState = MoveStateForInput(input);

        // Passe l'état de rotation au contrôleur de rotation
        RotationControleur controller = Rotation.GetComponent<RotationControleur>();
        controller.rotationState = rotationState;
    }

    // Définit l'état de rotation en fonction de l'entrée utilisateur et du sens de rotation
    EtatRotation MoveStateForInput(float input)
    {
        if (Sens == 1)
        {
            if (input > 0)
            {
                return EtatRotation.Positif;
            }
            else if (input < 0)
            {
                return EtatRotation.Negatif;
            }
            else
            {
                return EtatRotation.Fixe;
            }
        }
        else if (Sens == -1)
        {
            if (input < 0)
            {
                return EtatRotation.Positif;
            }
            else if (input > 0)
            {
                return EtatRotation.Negatif;
            }
            else
            {
                return EtatRotation.Fixe;
            }
        }
        else
        {
            return EtatRotation.Fixe;
        }
    }
}

