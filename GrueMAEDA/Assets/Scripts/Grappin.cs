using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grappin : MonoBehaviour
{
    // Événement de collision pour attacher le grappin à un objet avec ArticulationBody
    void OnCollisionEnter(Collision Collision)
    {
        // Si l'objet en collision a un ArticulationBody, ajouter un FixedJoint pour le connecter
        if (Collision.gameObject.GetComponent<ArticulationBody>() != null)
        {
            FixedJoint joint = this.gameObject.AddComponent<FixedJoint>();
            joint.connectedArticulationBody = Collision.articulationBody;
        }
    }

    // Désattache le grappin lorsque la touche espace est enfoncée
    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            Destroy(this.gameObject.GetComponent<FixedJoint>());
        }
    }
}
