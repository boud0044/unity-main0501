using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mouvement : MonoBehaviour
{
    void Update()
    {
        // Mouvement vers le bas avec la flèche bas
        if (Input.GetKey(KeyCode.DownArrow)) 
        {
            transform.Translate(Vector3.down * 0.005f);
        }

        // Mouvement vers le haut avec la flèche haut
        if (Input.GetKey(KeyCode.UpArrow)) 
        {
            transform.Translate(Vector3.up * 0.005f);
        }

        // Rotation à gauche avec la flèche gauche
        if (Input.GetKey(KeyCode.LeftArrow)) 
        {
            transform.Rotate(Vector3.forward, -0.2f);
        }

        // Rotation à droite avec la flèche droite
        if (Input.GetKey(KeyCode.RightArrow)) 
        {
            transform.Rotate(Vector3.forward, 0.2f);
        }
    }
}
