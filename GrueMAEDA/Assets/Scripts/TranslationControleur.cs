using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EtatTranslation { Fixe = 0, Positif = 1, Negatif = -1 };

public class TranslationControleur : MonoBehaviour
{
    public EtatTranslation moveState = EtatTranslation.Fixe;
    public float speed = 1.0f;

    private void FixedUpdate()
    {
        if (moveState != EtatTranslation.Fixe)
        {
            ArticulationBody articulation = GetComponent<ArticulationBody>();
            float xDrivePostion = articulation.jointPosition[0];
            float targetPosition = xDrivePostion + -(float)moveState * Time.fixedDeltaTime * speed;
            var drive = articulation.xDrive;
            drive.target = targetPosition;
            articulation.xDrive = drive;
        }
    }
}
