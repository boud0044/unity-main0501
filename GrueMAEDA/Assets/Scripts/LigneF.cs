using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LigneF : MonoBehaviour
{
    public GameObject Moufle; // Objet représentant la moufle
    public GameObject Crochet; // Objet représentant le crochet
    public Color lineColor = Color.black; // Couleur de la ligne

    // Dessine une ligne entre la moufle et le crochet dans chaque frame fixe
    void FixedUpdate()
    {   
        Vector3 mouflePos = Moufle.transform.position;
        Vector3 crochetPos = Crochet.transform.position;
        Debug.DrawLine(mouflePos, crochetPos, Color.black);
    }
}
