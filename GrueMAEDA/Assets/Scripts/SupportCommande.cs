using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SupportCommande : MonoBehaviour
{
    public GameObject[] supports;  // Array to hold the four supports
    public string supportAxis = "SupportController";  // Ensure the axis matches the Input Manager setup

    void Update()
    {
        float input = Input.GetAxis(supportAxis);  // Input axis is "SupportController"

        if (input > 0)  // Positive input (e.g., pressing 'l')
        {
            OpenSupports();
        }
        else if (input < 0)  // Negative input (e.g., pressing 'p')
        {
            CloseSupports();
        }
    }

    void OpenSupports()
    {
        foreach (GameObject support in supports)
        {
            SupportController controller = support.GetComponent<SupportController>();
            controller.supportState = EtatSupport.Positif;  // Set the state to extending
        }
    }

    void CloseSupports()
    {
        foreach (GameObject support in supports)
        {
            SupportController controller = support.GetComponent<SupportController>();
            controller.supportState = EtatSupport.Negatif;  // Set the state to retracting
        }
    }
}
