using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCamera : MonoBehaviour
{
    public List<Camera> cameras; // Liste des caméras à basculer
    private int currentCameraIndex = 0; // Index de la caméra actuellement active

    void Start()
    {
        // Assurer que seule la première caméra est active au démarrage
        for (int i = 0; i < cameras.Count; i++)
        {
            cameras[i].enabled = (i == currentCameraIndex);
        }
    }

    void Update()
    {
        // Vérifie si la touche "C" est appuyée
        if (Input.GetKeyDown(KeyCode.C))
        {
            // Désactive la caméra actuelle
            cameras[currentCameraIndex].enabled = false;

            // Incrémente l'index pour passer à la caméra suivante
            currentCameraIndex = (currentCameraIndex + 1) % cameras.Count;

            // Active la nouvelle caméra
            cameras[currentCameraIndex].enabled = true;
        }
    }
}
