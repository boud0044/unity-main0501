using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EtatRotation { Fixe = 0, Positif = 1, Negatif = -1 };

public class RotationControleur : MonoBehaviour
{
    public EtatRotation rotationState = EtatRotation.Fixe; // État de rotation actuel
    public float speed = 30.0f; // Vitesse de rotation

    private ArticulationBody articulation;

    // Initialisation de l'articulation
    void Start()
    {
        articulation = GetComponent<ArticulationBody>();
    }

    // Rotation de l'objet en fonction de l'état de rotation
    void FixedUpdate()
    {
        if (rotationState != EtatRotation.Fixe)
        {
            float rotationChange = (float)rotationState * speed * Time.fixedDeltaTime;
            float rotationGoal = CurrentPrimaryAxisRotation() + rotationChange;
            RotateTo(rotationGoal);
        }
    }

    // Obtient la rotation actuelle en degrés sur l'axe principal
    float CurrentPrimaryAxisRotation()
    {
        float currentRotationRads = articulation.jointPosition[0];
        float currentRotation = Mathf.Rad2Deg * currentRotationRads;
        return currentRotation;
    }

    // Définit la rotation cible de l'axe principal
    void RotateTo(float primaryAxisRotation)
    {
        var drive = articulation.xDrive;
        drive.target = primaryAxisRotation;
        articulation.xDrive = drive;
    }
}
