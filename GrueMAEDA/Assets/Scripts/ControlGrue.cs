using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlGrue : MonoBehaviour
{
    public float torque = 250f; // Force de couple pour le pivot
    public float forceChariot = 500f; // Force pour déplacer le chariot
    public float forceMoufle = 500f; // Force pour la moufle
    public ArticulationBody pivot;
    public ArticulationBody chariot;
    public ArticulationBody moufle;

    // Commande de déplacement pour la moufle
    void Update()
    {
        // Déplace la moufle vers le haut
        if (Input.GetKey(KeyCode.N))
        {
            moufle.AddRelativeForce(transform.up * forceMoufle);
        }
        // Déplace la moufle vers le bas
        if (Input.GetKey(KeyCode.B))
        {
            moufle.AddRelativeForce(transform.up * -forceMoufle);
        }
    }
}
